#!/bin/bash
USER="vagrant"
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get -y install awscli jq
if [ ! -d /home/$USER/.aws ]; then mkdir /home/$USER/.aws; fi
cp /vagrant/provision/credentials /home/$USER/.aws/
cat << EOF > /home/$USER/.aws/config
[default]
region = us-east-1
EOF
ln -s /vagrant/application/ /home/$USER/application
chown -R $USER:$USER /home/$USER/.aws /home/$USER/application
chmod 600 /home/$USER/.aws/*

if [ ! -x /usr/local/bin/kops ]; then
	echo "Installing kops ..."
	wget -q -O kops https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64
	chmod +x ./kops
	mv ./kops /usr/local/bin/
fi

if [ ! -x /usr/local/bin/kubectl ]; then
	echo "Installing kubectl ..."
	wget -q -O kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
	chmod +x ./kubectl
	mv ./kubectl /usr/local/bin/kubectl
	echo "source <(kubectl completion bash)" >> /home/$USER/.bashrc
fi

if [ ! -x /snap/bin/helm ]; then
	echo "Installing helm"
	snap install helm
fi

if [ ! -x /vagrant/provision/kops-aws.sh ]; then chmod 775 /vagrant/provision/kops-aws.sh; fi
if [ ! -x /vagrant/provision/clean-aws.sh ]; then chmod 775 /vagrant/provision/clean-aws.sh; fi
runuser -l vagrant -c "/vagrant/provision/kops-aws.sh $USER"


exit 0
