#!/bin/bash
# must be executed as 'vagrant' user

. $(dirname "$0")/environment

USER=$1
OUT="/home/$USER/.aws/credentials"

if ! (aws iam list-groups --profile default | jq '.Groups[].GroupName' | grep -q ${AWSGROUP}); then
	aws iam create-group --group-name ${AWSGROUP}
	aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonEC2FullAccess --group-name ${AWSGROUP}
	aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonRoute53FullAccess --group-name ${AWSGROUP}
	aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess --group-name ${AWSGROUP}
	aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/IAMFullAccess --group-name ${AWSGROUP}
	aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonVPCFullAccess --group-name ${AWSGROUP}
fi

if ! (aws iam list-users --profile default | jq '.Users[].UserName' | grep -q ${AWSGROUP}); then
	aws iam create-user --user-name ${AWSGROUP}
	aws iam add-user-to-group --user-name ${AWSGROUP} --group-name ${AWSGROUP}
fi

if ! (grep -q '\[kops]' /home/$USER/.aws/credentials); then
	echo "[kops]" >> $OUT
	aws iam create-access-key --user-name kops | jq -r '[.AccessKey.AccessKeyId, .AccessKey.SecretAccessKey] | "aws_access_key_id = \(.[0]) \naws_secret_access_key = \(.[1])"' >> $OUT

	cat << EOF >> /home/$USER/.aws/config
[kops]
region = us-east-1
EOF

	echo "export AWS_ACCESS_KEY_ID=$(aws configure get aws_access_key_id --profile kops)" >> /home/$USER/.bashrc
	echo "export AWS_SECRET_ACCESS_KEY=$(aws configure get aws_secret_access_key --profile kops)" >> /home/$USER/.bashrc
fi

if ! (aws s3api list-buckets --profile default | grep -q ${BUCKET}); then
 aws s3api create-bucket --bucket ${BUCKET} --region us-east-1
 aws s3api put-bucket-versioning --bucket ${BUCKET} --versioning-configuration Status=Enabled
fi

ssh-keygen -t rsa -f /home/$USER/.ssh/id_rsa -q -P ""

exit 0
