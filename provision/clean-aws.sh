#!/bin/bash

. $(dirname "$0")/environment

prof=default

if kubectl cluster-info; then
	echo "#######################################"
	echo "# First delete cluster with command   #"
	echo "#                                     #"
	echo "# kops delete cluster --name \${NAME}  #"
	echo "#                                     #"
	echo "#######################################"
	exit 3
fi

echo "Delete S3 bucket ..."
if [ $(aws s3 ls --profile default | grep ${BUCKET} | wc -l) -gt 0 ]; then 
	aws s3api delete-objects --bucket ${BUCKET} --delete --profile ${prof} "$(aws s3api list-object-versions --bucket ${buck} --profile ${prof} | jq '{Objects: [.Versions[] | {Key:.Key, VersionId : .VersionId}], Quiet: false}')" 

	aws s3 rb ${BUCKET} --force --profile ${prof}
	aws s3 rm ${BUCKET} --recursive --profile ${prof}
	aws s3api delete-bucket --bucket ${BUCKET} --profile ${prof}
fi

echo "Delete IAM group ..."
grs=("$(aws iam list-groups-for-user --user-name ${AWSGROUP} --profile ${prof} | jq -r '.Groups[] | .GroupName')")
if [[ "${#grs}" -gt "0" ]]; then
	for key in ${grs[@]}; do
    echo -e "\tRemove membership group ${key}"
		aws iam remove-user-from-group --user-name ${AWSGROUP} --group-name ${key} --profile ${prof}
	done
fi

attpol=("$(aws iam list-attached-group-policies --group-name ${AWSGROUP} --profile ${prof} | jq -r '.AttachedPolicies[] | .PolicyArn')")
if [[ "${#attpol}" -gt "0" ]]; then
	for key in ${attpol[@]}; do
    echo -e "\tDettach policy ${key}"
		aws iam detach-group-policy --group-name ${AWSGROUP} --policy-arn ${key} --profile ${prof}  
	done
fi
aws iam delete-group --group-name ${AWSGROUP} --profile ${prof}

echo "Delete IAM user ..."
keys=("$(aws iam list-access-keys --user-name "${AWSGROUP}" --profile ${prof} | jq -r '.AccessKeyMetadata[] | .AccessKeyId')")
if [[ "${#keys}" -gt "0" ]]; then
    # shellcheck disable=SC2068
    for key in ${keys[@]}; do
        echo -e "\tDeleting access key ${key}"
        aws iam delete-access-key --user-name ${AWSGROUP} --access-key-id ${key} --profile ${prof}
    done
fi
aws iam delete-user --user-name ${AWSGROUP} --profile ${prof}

echo "Delete local credentials ..."
cp /vagrant/provision/credentials ~/.aws/credentials
cat << EOF > /home/vagrant/.aws/config
[default]
region = us-east-1
EOF
sed -i.old '/.* AWS_/d' /home/vagrant/.bashrc

unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY

echo "Done!"
exit 0
