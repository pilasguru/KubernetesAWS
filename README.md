# Kubernetes at AWS

This is a Vagrant environment to creates a VM with credentials, tools and configuration to _play_ with [Kubernetes at AWS](https://github.com/kubernetes/kops/blob/master/docs/aws.md).

## Installation

### Previous requirements

Login AWS Console, create a IAM user with `AdministratorAccess` policy and obtain API credentials.

![User](images/IAM.png)

Copy file `provision/credentials-original`

```
cp provision/credentials-original provision/credentials
```

Set credentials data at new file `provision/credentials`

```
vim provision/credentials
```

Configure cluster `NAME` and `BUCKET` name at file:

```
vim provision/environment
```

### Deploy VM

Start VM

```
vagrant up
```

The deployment will install:

- [kops](https://github.com/kubernetes/kops)
- [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)
- [helm](https://docs.helm.sh/helm/)

Also create:

- AWS group `kops`
- AWS user `kops`
- S3 bucket `kubernetesaws-example-com-state-store`

_(names of user+group & bucket may be tuned at `provision/kops-aws.sh` variables)_

```
vagrant ssh
```

The user `vagrant` has an aws profile named `kops` with credentials


## Start with kubernetes

As `vagrant` user set your environment _(see `/vagrant/provision/environment` file)_

```
export NAME=myfirstcluster.k8s.local
export KOPS_STATE_STORE=s3://kubernetesaws-example-com-state-store
```

You may use another cluster name, but ensure ends with `.k8s.local`.

After cluster is created *Helm* need to be configured with `helm init` command.


Then continue with [create cluster configuration](https://github.com/kubernetes/kops/blob/master/docs/aws.md#create-cluster-configuration) and enjoy. 



## Clean environment

To start clean this environment you must first delete all your kubernetes clusters and other artifacts created by yourself. 

To clean resources created by _vagrant provision_ execute with `vagrant` user:

```
/vagrant/provision/clean-aws.sh
```

and then destoy VM as usual

```
vagrant destroy -f
```


## References

* [Kubernetes + AWS](https://github.com/kubernetes/kops/blob/master/docs/aws.md)
